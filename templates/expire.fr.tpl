MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
From: {{ .FromName }} <{{ .FromAddress }}>
To: {{ .ToName }} <{{ .ToAddress }}>
Subject: [J-{{ .ExpireDays }}] Notification d'expiration de votre mot de passe

Bonjour {{ .ToName }},

Votre mot de passe pour vous connecter à https://{{ .KeycloakHost }}/realms/{{ .KeycloakRealm }}/account/#/ expire le {{ .ExpireDate.Format "02 Jan 2006" }} ({{ .ExpireDays }} jours).

Pour des raisons de sécurité, nous vous recommandons de le mettre à jour avant cette date.

Merci de votre coopération.
