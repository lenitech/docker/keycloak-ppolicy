module gitlab.com/lenitech/docker/keycloak-ppolicy

go 1.21.8

require github.com/Nerzal/gocloak/v13 v13.9.0

require (
	github.com/go-resty/resty/v2 v2.16.5 // indirect
	github.com/golang-jwt/jwt/v5 v5.2.1 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/segmentio/ksuid v1.0.4 // indirect
	golang.org/x/net v0.35.0 // indirect
)
