package main

import (
	"bufio"
	"bytes"
	"embed"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"text/template"
	"time"
)

//go:embed templates
var tpl embed.FS

func sendNotifEmail(user string, email string, expiredDate *time.Time, expireDays int) {
	from := os.Getenv("SMTP_FROM")
	fromName := os.Getenv("SMTP_FROM_NAME")

	tmpl, err := template.New("").ParseFS(tpl, "templates/*.tpl")
	if err != nil {
		log.Panic(err)
	}
	var buf bytes.Buffer
	w := bufio.NewWriter(&buf)
	err = tmpl.ExecuteTemplate(w, "expire.fr.tpl", struct {
		FromName      string
		FromAddress   string
		ToName        string
		ToAddress     string
		KeycloakHost  string
		KeycloakRealm string
		ExpireDate    *time.Time
		ExpireDays    int
	}{
		FromName:      fromName,
		FromAddress:   from,
		ToName:        user,
		ToAddress:     email,
		KeycloakHost:  os.Getenv("KEYCLOAK_HOST"),
		KeycloakRealm: os.Getenv("KEYCLOAK_REALM"),
		ExpireDate:    expiredDate,
		ExpireDays:    expireDays,
	})
	if err != nil {
		log.Panic(err)
	}
	err = w.Flush()
	if err != nil {
		log.Panic(err)
	}

	err = smtp.SendMail(fmt.Sprintf("%s:%s", os.Getenv("SMTP_SERVER"), os.Getenv("SMTP_PORT")), nil, from, []string{email}, buf.Bytes())
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Email Sent Successfully!")
}
