package main

import (
	"context"
	"fmt"
	"github.com/Nerzal/gocloak/v13"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"
)

func main() {
	ctx := context.Background()
	realmName := os.Getenv("KEYCLOAK_REALM")
	client := gocloak.NewClient(os.Getenv("KEYCLOAK_URL"))
	log.Printf("Login to Keycloak with username and password")
	token, err := client.LoginAdmin(ctx, os.Getenv("KEYCLOAK_USERNAME"), os.Getenv("KEYCLOAK_PASSWORD"), realmName)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Successfully logged to Keycloak with username and password")
	realm, err := client.GetRealm(ctx, token.AccessToken, realmName)
	if err != nil {
		log.Panic(err)
	}
	if realm.PasswordPolicy == nil {
		log.Printf("No password policy defined !")
		return
	}
	pattern := regexp.MustCompile("forceExpiredPasswordChange\\(([0-9]+)\\)")
	subStr := pattern.FindStringSubmatch(*realm.PasswordPolicy)
	if len(subStr) < 1 {
		log.Printf("No password expiration defined !")
		return
	}
	log.Printf("Password expiration enabled with value %s day(s)", subStr[1])
	days, err := strconv.Atoi(subStr[1])
	if err != nil {
		log.Panic(err)
	}
	passwordMaxAge, err := time.ParseDuration(fmt.Sprintf("%sh", strconv.Itoa(days*24)))
	if err != nil {
		log.Panic(err)
	}
	users, err := client.GetUsers(ctx, token.AccessToken, realmName, gocloak.GetUsersParams{})
	if err != nil {
		log.Panic(err)
	}
	for _, user := range users {
		if !*user.Enabled {
			continue
		}
		var createdDate time.Time
		if user.FederationLink != nil {
			log.Printf("User %s is a federated user from %s, trying to get password expiration from pwdChangedTime attribute", *user.Email, *user.FederationLink)
			if *user.Attributes != nil {
				attrs := *user.Attributes
				if len(attrs["pwdChangedTime"]) > 0 {
					createdDate, err = time.Parse("20060102150405Z", attrs["pwdChangedTime"][0])
					if err != nil {
						log.Panic(err)
					}
				}
			}
		} else {
			log.Printf("User %s is a local Keycloak user, trying to get password expiration from password credential", *user.Email)
			credentials, err := client.GetCredentials(ctx, token.AccessToken, realmName, *user.ID)
			if err != nil {
				log.Panic(err)
			}
			for _, credential := range credentials {
				if *credential.Type == "password" {
					createdDate = time.UnixMilli(*credential.CreatedDate)
					break
				}
			}
		}
		if !createdDate.IsZero() {
			log.Printf("User %s password created or modified on %s", *user.Email, createdDate)
			expiredDate := createdDate.Add(passwordMaxAge)
			expireDays := int(expiredDate.Sub(time.Now()).Round(time.Hour).Hours() / 24)
			if expireDays == 14 || expireDays == 7 || expireDays == 2 || expireDays == 1 {
				if user.Email != nil {
					log.Printf("User %s (%s) : password is about to expire in %d days, send notification email", *user.Email, *user.Username, expireDays)
					sendNotifEmail(fmt.Sprintf("%s %s", *user.FirstName, *user.LastName), *user.Email, &expiredDate, expireDays)
				}
			}
		}
	}
}
