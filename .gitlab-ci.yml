---
include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  KO_VERSION: "v0.17.1"
  GOCACHE: "${CI_PROJECT_DIR}/.gocache"
  GIT_DEPTH: 1
  FF_USE_FASTZIP: 1
  CACHE_COMPRESSION_LEVEL: "fastest"

cache:
  key: "gocache"
  paths:
    - .gocache

stages:
  - analyze
  - test
  - build
  - dynamic-pipeline

sast:
  stage: analyze

gemnasium-dependency_scanning:
  stage: analyze

test:
  stage: test
  image: golang:1.24.1@sha256:c5adecdb7b3f8c5ca3c88648a861882849cc8b02fed68ece31e25de88ad13418
  rules:
    - when: always
  script:
    - go install github.com/jstemmer/go-junit-report@latest
    - go test -v ./... 2>&1 | tee /dev/stderr | /go/bin/go-junit-report -set-exit-code > report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
  needs: []

build:
  stage: build
  image: golang:1.24.1@sha256:c5adecdb7b3f8c5ca3c88648a861882849cc8b02fed68ece31e25de88ad13418
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        TAGS: "latest"
        VERSION: "${CI_COMMIT_SHORT_SHA}"
      when: on_success
    - if: $CI_COMMIT_TAG
      variables:
        TAGS: "${CI_COMMIT_TAG}"
        VERSION: "${CI_COMMIT_TAG}"
      when: on_success
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      variables:
        TAGS: "${CI_COMMIT_REF_SLUG}"
        VERSION: "${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_SLUG}"
      when: on_success
    - when: never
  variables:
    KO_DOCKER_REPO: "${CI_REGISTRY_IMAGE}"
  before_script:
    - export KO_VERSION_SLUG=$(echo "${KO_VERSION}"|tr -d 'v')
    - wget --quiet "https://github.com/google/ko/releases/download/${KO_VERSION}/ko_${KO_VERSION_SLUG}_Linux_x86_64.tar.gz" -O- | tar -C /usr/bin -xzf - ko
    - chmod 755 /usr/bin/ko
  script:
    - export SOURCE_DATE_EPOCH=$(date +%s)
    - export KO_DATA_DATE_EPOCH=$(git log -1 --format='%ct')
    - ko login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - ko publish --bare --tags "${TAGS}" ./
  needs: ["test"]
